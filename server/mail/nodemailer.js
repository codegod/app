import bunyan from "bunyan";
import path from "path";
import nodemailer from "nodemailer";
import handlebars from "express-handlebars";
import nodemailerExpressHandlebars from "nodemailer-express-handlebars";
import config from "../config/mail";

const appMailer = {};

appMailer.transporter = nodemailer.createTransport(
  {
    service: config.service,
    auth: {
      user: config.user,
      pass: config.password
    },
    logger: bunyan.createLogger({
      name: "nodemailer"
    }),
    debug: true
  },
  {
    from: `${config.localservicename} <${config.localserviceemail}>`,
    headers: {
      "X-Laziness-level": 1000
    }
  }
);

appMailer.viewEngine = handlebars.create({});

appMailer.sut = nodemailerExpressHandlebars({
  viewEngine: appMailer.viewEngine,
  viewPath: path.resolve(__dirname, "../mail/templates/")
});

appMailer.sendMail = function sendMail(message) {
  this.transporter.use("compile", this.sut);
  this.transporter.sendMail(message, (error, info) => {
    if (error) {
      console.log(`Error occurred: ${error.message}`);
      throw new Error("Email not sent");
    }
    console.log(`Success, server responded with ${info.response}`);
    this.transporter.close();
  });
};

// Message example
// const mail = {
//   from: "cuciurca@gmail.com",
//   to: "constantincuciurca@gmail.com",
//   subject: "Test",
//   template: "validation",
//   context: {
//     name: "Name"
//   }
// };

export default appMailer;
