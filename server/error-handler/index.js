import CreateError from "http-errors";

export default {
  CourseNotFound: new CreateError(404, "Course not found."),
  BadFields: new CreateError(400, "Fill out fields correctly"),
  UserNotFound: new CreateError(404, "User not found."),
  LessonNotFound: new CreateError(404, "This lesson not found."),
  ChapterNotFound: new CreateError(404, "Chapter not found."),
  CategoryNotFound: new CreateError(404, "Category not found.")
};
