import { Model } from "sequelize";

export default (sequelize, DataTypes) => {
  class Role extends Model {
    static associate(models) {
      Role.belongsToMany(models.User, { through: "UserRoles" });
      Role.belongsToMany(models.Privilege, { through: "RolePrivileges" });
    }
  }
  Role.init(
    {
      role: DataTypes.STRING,
      description: DataTypes.STRING
    },
    { sequelize, onDelete: "CASCADE" }
  );
  return Role;
};
