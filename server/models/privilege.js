import { Model } from "sequelize";

export default (sequelize, DataTypes) => {
  class Privilege extends Model {
    static associate(models) {
      Privilege.belongsToMany(models.Role, { through: "RolePrivileges" });
    }
  }
  Privilege.init(
    {
      privilege: DataTypes.STRING,
      description: DataTypes.STRING
    },
    { sequelize, onDelete: "CASCADE" }
  );
  return Privilege;
};
