import { Model } from "sequelize";

export default (sequelize, DataTypes) => {
  class Course extends Model {
    static associate(models) {
      Course.belongsTo(models.User, {
        as: "Author",
        foreignKey: "authorId"
      });
      Course.belongsTo(models.Category);
      Course.hasMany(models.Chapter);
      Course.belongsToMany(models.User, {
        as: "Subscribers",
        through: "UserSubscriptions"
      });
    }
  }
  Course.init(
    {
      id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      course: { type: DataTypes.STRING, i18n: true }
    },
    { sequelize, onDelete: "CASCADE" }
  );
  return Course;
};
