import { Model } from "sequelize";

export default (sequelize, DataTypes) => {
  class Comment extends Model {
    static associate(models) {
      Comment.belongsTo(models.User, {
        as: "Author",
        foreignKey: "authorId"
      });
      Comment.belongsTo(models.Lesson);
    }
  }
  Comment.init(
    {
      comment: DataTypes.STRING
    },
    { sequelize, onDelete: "CASCADE" }
  );
  return Comment;
};
