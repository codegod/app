import { Model } from "sequelize";

export default (sequelize, DataTypes) => {
  class Category extends Model {
    static associate(models) {
      Category.hasMany(models.Course);
    }
  }
  Category.init(
    {
      id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      category: { type: DataTypes.STRING, i18n: true }
    },
    { sequelize, onDelete: "CASCADE" }
  );

  return Category;
};
