import bcrypt from "bcrypt-nodejs";
import { Model } from "sequelize";

export default (sequelize, DataTypes) => {
  class User extends Model {
    static associate(models) {
      User.hasMany(models.Course, { foreignKey: "authorId" });
      User.belongsToMany(models.Course, {
        as: "Subscriptions",
        through: "UserSubscriptions"
      });
      User.belongsToMany(models.Role, { through: "UserRoles" });
    }
    comparePasswords(password) {
      return bcrypt.compareSync(password, this.password);
    }
  }
  User.init(
    {
      name: {
        type: DataTypes.STRING
      },
      username: {
        type: DataTypes.STRING,
        unique: {
          args: true,
          message: "Username must be unique, this already exists.",
          fields: [sequelize.fn("lower", sequelize.col("username"))]
        },
        validate: {
          min: {
            args: 3,
            msg:
              "Username must start with a letter, have no spaces, and be at least 3 characters."
          },
          max: {
            args: 40,
            msg:
              "Username must start with a letter, have no spaces, and be at less than 40 characters."
          },
          is: {
            args: /^[A-Za-z][A-Za-z0-9-]+$/i,
            msg:
              "Username must start with a letter, have no spaces, and be 3 - 40 characters."
          }
        }
      },
      email: {
        type: DataTypes.STRING,
        unique: {
          args: true,
          msg:
            "Oops. Looks like you already have an account with this email address. Please try to login.",
          fields: [sequelize.fn("lower", sequelize.col("email"))]
        },
        validate: {
          isEmail: {
            args: true,
            msg: "The email you entered is invalid or is already in our system."
          },
          max: {
            args: 254,
            msg:
              "The email you entered is invalid or longer than 254 characters."
          }
        }
      },
      password: {
        type: DataTypes.STRING
      },
      isConfirmed: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },
      isActive: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
      },
      temporaryToken: {
        type: DataTypes.STRING,
        defaultValue: true
      }
    },
    {
      sequelize,
      hooks: {
        beforeCreate: user => {
          if (user.changed("password")) {
            user.password = bcrypt.hashSync(user.password);
          }
        },
        beforeUpdate: user => {
          if (user.changed("password")) {
            user.password = bcrypt.hashSync(user.password);
          }
        }
      },
      indexes: [
        {
          unique: true,
          fields: ["username", "email"]
        }
      ],
      onDelete: "CASCADE"
    }
  );

  return User;
};
