import { Model } from "sequelize";

export default (sequelize, DataTypes) => {
  class Chapter extends Model {
    static associate(models) {
      Chapter.belongsTo(models.Course);
      Chapter.hasMany(models.Lesson);
    }
  }
  Chapter.init(
    {
      id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      chapter: { type: DataTypes.STRING, i18n: true }
    },
    { sequelize, onDelete: "CASCADE" }
  );

  return Chapter;
};
