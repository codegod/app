import { Model } from "sequelize";

export default (sequelize, DataTypes) => {
  class Attachment extends Model {
    static associate(models) {
      Attachment.belongsTo(models.Lesson);
    }
  }
  Attachment.init(
    {
      id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      address: DataTypes.STRING,
      description: { type: DataTypes.STRING, i18n: true }
    },
    { sequelize, onDelete: "CASCADE" }
  );
  return Attachment;
};
