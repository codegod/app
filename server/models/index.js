import fs from "fs";
import path from "path";
import Sequelize from "sequelize";
import SequelizeI18N from "sequelize-i18n";
import configs from "../config/database";

const basename = path.basename(module.filename);
const env = process.env.NODE_ENV || "development";
const config = configs[env];
const db = {};
let sequelize = {};
const languages = {
  list: ["en", "fr", "md"],
  default: "en"
};

if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable]);
} else {
  sequelize = new Sequelize(
    config.database,
    config.username,
    config.password,
    config
  );
}

const i18n = new SequelizeI18N(sequelize, {
  languages: languages.list,
  default_language: languages.default
});

i18n.init();
fs
  .readdirSync(__dirname)
  .filter(
    file =>
      file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
  )
  .forEach(file => {
    const model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
  i18n.setInstanceMethods(db[modelName].prototype, i18n.getI18nName(modelName));
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

// db.sequelize.sync({ force: true });
// db.sequelize.sync();
// db.sequelize.sync({ alter: true });

module.exports = db;
