import { Model } from "sequelize";

export default (sequelize, DataTypes) => {
  class Lesson extends Model {
    static associate(models) {
      Lesson.belongsTo(models.Chapter);
      Lesson.hasMany(models.Attachment);
      Lesson.hasMany(models.Comment);
    }
  }
  Lesson.init(
    {
      id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      lesson: { type: DataTypes.STRING, i18n: true },
      address: DataTypes.STRING
    },
    { sequelize, onDelete: "CASCADE" }
  );
  return Lesson;
};
