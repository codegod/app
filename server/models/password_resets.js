import { Model } from "sequelize";

export default (sequelize, DataTypes) => {
  class PasswordResets extends Model {}
  PasswordResets.init(
    {
      email: DataTypes.STRING,
      token: DataTypes.STRING
    },
    { sequelize }
  );
  return PasswordResets;
};
