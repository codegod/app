import passport from "passport";
import { Strategy as JwtStrategy, ExtractJwt } from "passport-jwt";
import config from "../config/app";
import { User } from "../models";

const opts = {
  jwtFromRequest: ExtractJwt.fromAuthHeader(),
  secretOrKey: config.key
};

const strategy = new JwtStrategy(opts, (payload, done) => {
  User.findOne({
    where: { id: payload.id },
    attributes: ["id", "username", "email", "name"]
  }).then(user => {
    if (!user) done(new Error("Invalid token."), null);
    done(null, user);
  });
});
passport.use(strategy);

export default {
  initialize: () => passport.initialize(),
  authenticate: () => passport.authenticate("jwt", { session: false })
};
