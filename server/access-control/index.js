import CreateError from "http-errors";
import { User, Role, Privilege } from "../models";

export const PBAC = options => (req, res, next) => {
  User.findOne({
    where: { id: req.user.id },
    include: [{ model: Role, include: Privilege }]
  })
    .then(user => {
      const userPrivileges = user.Roles
        .map(role => role.Privileges.map(privilege => privilege.privilege))
        .reduce((a, b) => a.concat(b), []);
      [].concat(options).forEach(option => {
        if (userPrivileges.indexOf(option) === -1)
          throw new CreateError.Forbidden();
      });
      next();
    })
    .catch(err => {
      next(err);
    });
};
export const RBAC = options => (req, res, next) => {
  User.findOne({
    where: { id: req.user.id },
    include: [Role]
  })
    .then(user => {
      const userRoles = user.Roles.map(role => role.role);
      [].concat(options).forEach(option => {
        if (userRoles.indexOf(option) === -1) throw new CreateError.Forbidden();
      });
      next();
    })
    .catch(err => {
      next(err);
    });
};
