"use strict";

const Courses = [
  {
    id: 1,
    createdAt: new Date(),
    updatedAt: new Date(),
    CategoryId: 1,
    authorId: 1
  },
  {
    id: 2,
    createdAt: new Date(),
    updatedAt: new Date(),
    CategoryId: 2,
    authorId: 1
  },
  {
    id: 3,
    createdAt: new Date(),
    updatedAt: new Date(),
    CategoryId: 3,
    authorId: 1
  }
];

const Course_i18ns = [
  {
    id: 1,
    language_id: "en",
    parent_id: 1,
    course: "OOP Programing Course"
  },
  {
    id: 2,
    language_id: "fr",
    parent_id: 1,
    course: "OOP Programation Cours"
  },
  {
    id: 3,
    language_id: "en",
    parent_id: 2,
    course: "ES6 Code style Course"
  },
  {
    id: 4,
    language_id: "fr",
    parent_id: 2,
    course: "Cours pour ES6 style de code"
  },
  {
    id: 5,
    language_id: "en",
    parent_id: 3,
    course: "Simple Query Language Course"
  },
  {
    id: 6,
    language_id: "fr",
    parent_id: 3,
    course: "Cours pour le Simple Language de Requetes"
  }
];

module.exports = {
  up: function(queryInterface, Sequelize) {
    queryInterface.bulkInsert("Courses", Courses, {})
    return queryInterface.bulkInsert("Course_i18ns", Course_i18ns, {})
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("Courses", null, {})
  }
};
