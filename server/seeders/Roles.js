"use strict";

const Roles = [
  {
    id: 1,
    role: "Admin",
    description: "Admin role - all privileges",
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: 2,
    role: "Editor",
    description: "Editor role",
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: 3,
    role: "Author",
    description: "Author role",
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: 4,
    role: "Contributor",
    description: "Contributor role",
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: 5,
    role: "Subscriber",
    description: "Subscriber role",
    createdAt: new Date(),
    updatedAt: new Date()
  }
];

module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.bulkInsert("Roles", Roles, {});
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("Roles", null, {});
  }
};
