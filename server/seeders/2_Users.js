"use strict";

const Users = [
  {
    id: 1,
    name: "Admin",
    username: "administrator",
    email: "admin@test.com",
    password: "$2a$10$c/KrZyLntI4NjJGOLyLDGe7F3ml100wF4ygx5HcnOezCx9zxsnToK",
    isConfirmed: 1,
    isActive: 1,
    createdAt: "2017-06-26 07:31:44",
    updatedAt: "2017-06-26 07:31:44"
  },
  {
    id: 2,
    name: "Esteve",
    username: "estevejo",
    email: "estevejo@gmail.com",
    password: "$2a$10$c/KrZyLntI4NjJGOLyLDGe7F3ml100wF4ygx5HcnOezCx9zxsnToK",
    isConfirmed: 1,
    isActive: 1,
    createdAt: "2017-06-26 07:31:44",
    updatedAt: "2017-06-26 07:31:44"
  },
  {
    id: 3,
    name: "Edwin",
    username: "wesbos",
    email: "wesbos@test.com",
    password: "$2a$10$c/KrZyLntI4NjJGOLyLDGe7F3ml100wF4ygx5HcnOezCx9zxsnToK",
    isConfirmed: 1,
    isActive: 1,
    createdAt: "2017-06-26 07:31:44",
    updatedAt: "2017-06-26 07:31:44"
  }
];

module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.bulkInsert("Users", Users, {});
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("Users", null, {});
  }
};
