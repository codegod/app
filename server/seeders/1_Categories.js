"use strict";

const Categories = [
  {
    id: 1,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: 2,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: 3,
    createdAt: new Date(),
    updatedAt: new Date()
  }
];

const Category_i18ns = [
  {
    id: 1,
    language_id: "en",
    parent_id: 1,
    category: "OOP Programing"
  },
  {
    id: 2,
    language_id: "fr",
    parent_id: 1,
    category: "OOP Programation"
  },
  {
    id: 3,
    language_id: "en",
    parent_id: 2,
    category: "ES6 Code style"
  },
  {
    id: 4,
    language_id: "fr",
    parent_id: 2,
    category: "ES6 style de code"
  },
  {
    id: 5,
    language_id: "en",
    parent_id: 3,
    category: "Simple Query Language"
  },
  {
    id: 6,
    language_id: "fr",
    parent_id: 3,
    category: "Simple Language de Requetes"
  }
];

module.exports = {
  up: function(queryInterface, Sequelize) {
    queryInterface.bulkInsert("Categories", Categories, {})
    return queryInterface.bulkInsert("Category_i18ns", Category_i18ns, {})
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("Categories", null, {})
  }
};
