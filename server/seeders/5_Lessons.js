"use strict";
const uuid = require("uuid/v4");

const Lessons = [
  {
    id: 1,
    address: "/uploads/lessons/lesson-"+uuid()+".mp4",
    createdAt: new Date(),
    updatedAt: new Date(),
    ChapterId: 1
  },
  {
    id: 2,
    address: "/uploads/lessons/lesson-"+uuid()+".mp4",
    createdAt: new Date(),
    updatedAt: new Date(),
    ChapterId: 2
  },
  {
    id: 3,
    address: "/uploads/lessons/lesson-"+uuid()+".mp4",
    createdAt: new Date(),
    updatedAt: new Date(),
    ChapterId: 3
  },
  {
    id: 4,
    address: "/uploads/lessons/lesson-"+uuid()+".mp4",
    createdAt: new Date(),
    updatedAt: new Date(),
    ChapterId: 4
  },
  {
    id: 5,
    address: "/uploads/lessons/lesson-"+uuid()+".mp4",
    createdAt: new Date(),
    updatedAt: new Date(),
    ChapterId: 5
  }
];

const Lesson_i18ns = [
  {
    id: 1,
    language_id: "en",
    parent_id: 1,
    lesson: "Creating class Mouse"
  },
  {
    id: 2,
    language_id: "fr",
    parent_id: 1,
    lesson: "Creer le classe Mouse"
  },
  {
    id: 3,
    language_id: "en",
    parent_id: 2,
    lesson: "How work inheritance"
  },
  {
    id: 4,
    language_id: "fr",
    parent_id: 2,
    lesson: "Comme lheritage du travail"
  },
  {
    id: 5,
    language_id: "en",
    parent_id: 3,
    lesson: "What is abstraction?"
  },
  {
    id: 6,
    language_id: "fr",
    parent_id: 3,
    lesson: "Quest-ce abstraction?"
  },
  {
    id: 7,
    language_id: "en",
    parent_id: 4,
    lesson: "Quest-ce abstraction?"
  },
  {
    id: 8,
    language_id: "fr",
    parent_id: 4,
    lesson: "Object Matching, Shorthand Notation"
  },
  {
    id: 9,
    language_id: "en",
    parent_id: 5,
    lesson: "Parameter Context Matching"
  },
  {
    id: 10,
    language_id: "fr",
    parent_id: 5,
    lesson: "Context de la parametre"
  }
];

module.exports = {
  up: function(queryInterface, Sequelize) {
    queryInterface.bulkInsert("Lessons", Lessons, {})
    return queryInterface.bulkInsert("Lesson_i18ns", Lesson_i18ns, {})
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("Lessons", null, {})
  }
};
