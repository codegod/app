"use strict";

const RolesPrivileges = [
  {
    roleId: 1,
    privilegeId: 1,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    roleId: 1,
    privilegeId: 2,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    roleId: 1,
    privilegeId: 3,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    roleId: 1,
    privilegeId: 4,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    roleId: 1,
    privilegeId: 5,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    roleId: 1,
    privilegeId: 6,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    roleId: 1,
    privilegeId: 7,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    roleId: 1,
    privilegeId: 8,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    roleId: 2,
    privilegeId: 2,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    roleId: 2,
    privilegeId: 3,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    roleId: 2,
    privilegeId: 4,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    roleId: 2,
    privilegeId: 5,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    roleId: 2,
    privilegeId: 6,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    roleId: 2,
    privilegeId: 7,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    roleId: 2,
    privilegeId: 8,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    roleId: 3,
    privilegeId: 4,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    roleId: 3,
    privilegeId: 5,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    roleId: 3,
    privilegeId: 6,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    roleId: 3,
    privilegeId: 7,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    roleId: 4,
    privilegeId: 5,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    roleId: 4,
    privilegeId: 6,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    roleId: 4,
    privilegeId: 7,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    roleId: 5,
    privilegeId: 7,
    createdAt: new Date(),
    updatedAt: new Date()
  }
];

module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.bulkInsert("RolePrivileges", RolesPrivileges, {});
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("RolePrivileges", null, {});
  }
};
