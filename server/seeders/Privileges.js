"use strict";

const Privileges = [
  {
    id: 1,
    privilege: "manage_users",
    description: "Manage another users, update or delete",
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: 2,
    privilege: "edit_published_posts",
    description: "Edit published posts",
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: 3,
    privilege: "edit_other_posts",
    description: "Edit posts of other users",
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: 4,
    privilege: "upload_files",
    description: "Upload files in system",
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: 5,
    privilege: "edit_own_posts",
    description: "Edit own posts",
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: 6,
    privilege: "write_own_posts",
    description: "Write own posts",
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: 7,
    privilege: "edit_own_profile",
    description: "Edit own profile",
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: 8,
    privilege: "edit_other_profile",
    description: "Edit other profile",
    createdAt: new Date(),
    updatedAt: new Date()
  }
];

module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.bulkInsert("Privileges", Privileges, {});
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("Privileges", null, {});
  }
};
