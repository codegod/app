"use strict";

const Comments = [
  {
    id: 1,
    comment: "Very very nice",
    authorId: 1,
    LessonId: 1,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: 2,
    comment: "Very very nice, and cool explained",
    authorId: 2,
    LessonId: 1,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: 3,
    comment: "Aujourd'hui je peut faire mes etudes, merci beaucoup!",
    authorId: 3,
    LessonId: 1,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: 4,
    comment: "Aujourd'hui je peut faire mes etudes, merci beaucoup!",
    authorId: 3,
    LessonId: 2,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: 5,
    comment: "Aujourd'hui je peut faire mes etudes, merci beaucoup!",
    authorId: 3,
    LessonId: 3,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: 6,
    comment: "Aujourd'hui je peut faire mes etudes, merci beaucoup!",
    authorId: 3,
    LessonId: 4,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: 7,
    comment: "Aujourd'hui je peut faire mes etudes, merci beaucoup!",
    authorId: 3,
    LessonId: 5,
    createdAt: new Date(),
    updatedAt: new Date()
  }
];

module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.bulkInsert("Comments", Comments, {});
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("Comments", null, {});
  }
};
