"use strict";

const Chapters = [
  {
    id: 1,
    createdAt: new Date(),
    updatedAt: new Date(),
    CourseId: 1
  },
  {
    id: 2,
    createdAt: new Date(),
    updatedAt: new Date(),
    CourseId: 1
  },
  {
    id: 3,
    createdAt: new Date(),
    updatedAt: new Date(),
    CourseId: 1
  },
  {
    id: 4,
    createdAt: new Date(),
    updatedAt: new Date(),
    CourseId: 2
  },
  {
    id: 5,
    createdAt: new Date(),
    updatedAt: new Date(),
    CourseId: 2
  },
  {
    id: 6,
    createdAt: new Date(),
    updatedAt: new Date(),
    CourseId: 3
  },
  {
    id: 7,
    createdAt: new Date(),
    updatedAt: new Date(),
    CourseId: 3
  },
  {
    id: 8,
    createdAt: new Date(),
    updatedAt: new Date(),
    CourseId: 3
  },
  {
    id: 9,
    createdAt: new Date(),
    updatedAt: new Date(),
    CourseId: 3
  }
];

const Chapter_i18ns = [
  {
    id: 1,
    language_id: "en",
    parent_id: 1,
    chapter: "Creating classes in OOP"
  },
  {
    id: 2,
    language_id: "fr",
    parent_id: 1,
    chapter: "Creer les classes en POO"
  },
  {
    id: 3,
    language_id: "en",
    parent_id: 2,
    chapter: "Inheritance"
  },
  {
    id: 4,
    language_id: "fr",
    parent_id: 2,
    chapter: "Heritage"
  },
  {
    id: 5,
    language_id: "en",
    parent_id: 3,
    chapter: "Abstraction"
  },
  {
    id: 6,
    language_id: "fr",
    parent_id: 3,
    chapter: "Abstraction"
  },
  {
    id: 7,
    language_id: "en",
    parent_id: 4,
    chapter: "Destructuring Assignment"
  },
  {
    id: 8,
    language_id: "fr",
    parent_id: 4,
    chapter: "Affectation destructrice"
  },
  {
    id: 9,
    language_id: "en",
    parent_id: 5,
    chapter: "Template Literals"
  },
  {
    id: 10,
    language_id: "fr",
    parent_id: 5,
    chapter: "Modele literaire"
  },
  {
    id: 11,
    language_id: "en",
    parent_id: 6,
    chapter: "Intro"
  },
  {
    id: 12,
    language_id: "fr",
    parent_id: 6,
    chapter: "Introduire"
  },
  {
    id: 13,
    language_id: "en",
    parent_id: 7,
    chapter: "Clauses"
  },
  {
    id: 14,
    language_id: "fr",
    parent_id: 7,
    chapter: "Provisions"
  },
  {
    id: 15,
    language_id: "en",
    parent_id: 8,
    chapter: "Data types"
  },
  {
    id: 16,
    language_id: "fr",
    parent_id: 8,
    chapter: "Types de donnees"
  },
  {
    id: 17,
    language_id: "en",
    parent_id: 9,
    chapter: "References"
  },
  {
    id: 18,
    language_id: "fr",
    parent_id: 9,
    chapter: "Les references"
  }
];

module.exports = {
  up: function(queryInterface, Sequelize) {
    queryInterface.bulkInsert("Chapters", Chapters, {})
    return queryInterface.bulkInsert("Chapter_i18ns", Chapter_i18ns, {})
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("Chapters", null, {})
  }
};
