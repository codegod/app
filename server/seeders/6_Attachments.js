"use strict";
const uuid = require("uuid/v4");

const Attachments = [
  {
    id: 1,
    address: "/uploads/attachments/attachment-"+uuid()+".rar",
    createdAt: new Date(),
    updatedAt: new Date(),
    LessonId: 1
  },
  {
    id: 2,
    address: "/uploads/attachments/attachment-"+uuid()+".rar",
    createdAt: new Date(),
    updatedAt: new Date(),
    LessonId: 2
  },
  {
    id: 3,
    address: "/uploads/attachments/attachment-"+uuid()+".rar",
    createdAt: new Date(),
    updatedAt: new Date(),
    LessonId: 3
  },
  {
    id: 4,
    address: "/uploads/attachments/attachment-"+uuid()+".rar",
    createdAt: new Date(),
    updatedAt: new Date(),
    LessonId: 4
  },
  {
    id: 5,
    address: "/uploads/attachments/attachment-"+uuid()+".rar",
    createdAt: new Date(),
    updatedAt: new Date(),
    LessonId: 5
  }
];

const Attachment_i18ns = [
  {
    id: 1,
    language_id: "en",
    parent_id: 1,
    description: "Creating class Mouse"
  },
  {
    id: 2,
    language_id: "fr",
    parent_id: 1,
    description: "Creer le classe Mouse"
  },
  {
    id: 3,
    language_id: "en",
    parent_id: 2,
    description: "How work inheritance"
  },
  {
    id: 4,
    language_id: "fr",
    parent_id: 2,
    description: "Comme l'heritage du travail"
  },
  {
    id: 5,
    language_id: "en",
    parent_id: 3,
    description: "What is abstraction?"
  },
  {
    id: 6,
    language_id: "fr",
    parent_id: 3,
    description: "Quest-ce abstraction?"
  },
  {
    id: 7,
    language_id: "en",
    parent_id: 4,
    description: "Quest-ce abstraction?"
  },
  {
    id: 8,
    language_id: "fr",
    parent_id: 4,
    description: "Object Matching, Shorthand Notation"
  },
  {
    id: 9,
    language_id: "en",
    parent_id: 5,
    description: "Parameter Context Matching"
  },
  {
    id: 10,
    language_id: "fr",
    parent_id: 5,
    description: "Context de la parametre"
  }
];

module.exports = {
  up: function(queryInterface, Sequelize) {
    queryInterface.bulkInsert("Attachments", Attachments, {})
    return queryInterface.bulkInsert("Attachment_i18ns", Attachment_i18ns, {})
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("Attachments", null, {})
  }
};
