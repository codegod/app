import express from "express";
import * as jwt from "jsonwebtoken";
import CreateError from "http-errors";
import { User, PasswordResets } from "../models";
import config from "../config/app";
import appMailer from "../mail/nodemailer";
import auth from "../passport/jwt";
import { PBAC as hasPrivilege } from "../access-control";
import HttpErrors from "../error-handler";

const router = express.Router();
const isValidUserData = userData => {
  if (
    userData.username === undefined ||
    userData.name === undefined ||
    userData.email === undefined ||
    userData.password === undefined ||
    userData.username === "" ||
    userData.name === "" ||
    userData.email === "" ||
    userData.password === ""
  )
    throw new CreateError(400, "Please fill out fields corectly.");
  return true;
};
const isValidUsername = (req, res, next) => {
  User.findOne({ where: { username: req.body.username } })
    .then(user => {
      if (user) throw new CreateError(406, "This username allready exists.");
      next();
    })
    .catch(err => {
      next(err);
    });
};

router
  .post("/register", (req, res, next) => {
    const userData = {
      name: req.body.name,
      username: req.body.username,
      email: req.body.email,
      password: req.body.password
    };
    userData.temporaryToken = jwt.sign(
      { username: userData.username, email: userData.email },
      config.key,
      { expiresIn: "24h" }
    );
    if (isValidUserData(userData))
      User.create(userData)
        .then(user => {
          if (user) {
            appMailer.sendMail({
              to: `${user.name}, <${user.email}>`,
              subject: "Account validation",
              template: "validation",
              context: {
                name: user.name,
                appName: config.name,
                server: config.server,
                temporarytoken: user.temporaryToken
              }
            });
            res
              .status(201)
              .send(
                "You are registered successfuly, check email for validation link."
              );
          }
        })
        .catch(err => {
          next(err);
        });
  })
  .post("/authenticate", (req, res, next) => {
    User.findOne({
      where: { username: req.body.username },
      attributes: ["id", "email", "isConfirmed", "username", "password"]
    })
      .then(user => {
        if (!user) throw new CreateError(400, "Wrong login.");
        if (!req.body.password)
          throw new CreateError(400, "Enter your passowrd!");
        if (!user.comparePasswords(req.body.password))
          throw new CreateError(400, "Wrong Password");
        if (!user.isConfirmed)
          throw new CreateError(
            423,
            "You need to confirm your profile. Check your e-mail."
          );
        const token = jwt.sign(
          { id: user.id, username: user.username, email: user.email },
          config.key,
          { expiresIn: "24h" }
        );
        res.status(201).send(token);
      })
      .catch(err => {
        next(err);
      });
  })
  .get("/resetusername/:email", (req, res, next) => {
    User.findOne({
      where: { email: req.params.email },
      attributes: ["email", "name", "username"]
    })
      .then(user => {
        if (!user) throw new CreateError(404, "E-mail was not found");
        appMailer.sendMail({
          to: `${user.name}, <${user.email}>`,
          subject: "Username Request",
          template: "resetUsername",
          context: {
            name: user.name,
            appName: config.name,
            userName: user.username
          }
        });
        res
          .status(202)
          .send("Username has been sent to e-mail, check it out! ");
      })
      .catch(err => {
        next(err);
      });
  })
  .put("/resetpassword", (req, res, next) => {
    if (!req.body.username) throw new CreateError(400, "Enter username");
    User.findOne({
      where: { username: req.body.username },
      attributes: ["username", "name", "isConfirmed", "email"]
    })
      .then(user => {
        if (!user) throw new CreateError(404, "Username was not found");
        if (!user.isConfirmed)
          throw new CreateError(403, "Account has not yet been activated");
        return PasswordResets.create({
          email: user.email,
          token: jwt.sign(
            { username: user.username, email: user.email },
            config.key,
            { expiresIn: "24h" }
          )
        });
      })
      .then(createdReset => {
        appMailer.sendMail({
          to: createdReset.email,
          subject: "Reset Password Request",
          template: "resetPassword",
          context: {
            appName: config.name,
            server: config.server,
            token: createdReset.token
          }
        });
        res
          .status(202)
          .send("Please check your e-mail for password reset link");
      })
      .catch(err => {
        next(err);
      });
  })
  .put("/savepassword", (req, res, next) => {
    PasswordResets.findOne({
      where: { token: req.body.token }
    })
      .then(passwordReset => {
        if (!passwordReset) throw new CreateError(400, "Invalid token");
        return User.findOne({
          where: { email: passwordReset.email },
          attributes: ["id", "name", "email", "password"]
        });
      })
      .then(user => {
        let decoded;
        try {
          decoded = jwt.verify(req.body.token, config.key);
        } catch (e) {
          next(e);
        }
        if (!decoded) throw new CreateError(406, "Password link has expired");
        if (!req.body.password)
          throw new CreateError(400, "Enter new password.");
        return user.updateAttributes({
          password: req.body.password
        });
      })
      .then(user => {
        appMailer.sendMail({
          to: `${user.name}, <${user.email}>`,
          subject: "Password recently reseted",
          template: "notifyResetPassword",
          context: {
            appName: config.name,
            name: user.name
          }
        });
        PasswordResets.destroy({ where: { email: user.email } });
        res.status(200).send("Password reseted successfully.");
      })
      .catch(err => {
        next(err);
      });
  })
  .use(auth.initialize(), auth.authenticate())
  .post("/profile", (req, res) => {
    res.json(req.user);
  })
  .put(
    "/edit",
    hasPrivilege("edit_own_profile"),
    isValidUsername,
    (req, res, next) => {
      User.findById(req.user.id)
        .then(user => {
          const userData = req.body;
          if (!userData.username && !userData.name)
            throw new CreateError(400, "Enter your new dates.");
          return user.updateAttributes(userData).then(() => {
            res.status(200).send("User edited successfully.");
          });
        })
        .catch(err => {
          next(err);
        });
    }
  )
  .put(
    "/edit/:username",
    hasPrivilege("edit_other_profile"),
    isValidUsername,
    (req, res, next) => {
      User.findOne({ where: { username: req.params.username } })
        .then(user => {
          const userData = req.body;
          if (!userData.username && !userData.name)
            throw new CreateError(400, "Enter your new dates.");
          user.updateAttributes(userData).then(() => {
            res.status(200).send("User edited successfully!");
          });
        })
        .catch(err => {
          next(err);
        });
    }
  )
  .get("/:username", (req, res, next) => {
    User.findOne({ where: { username: req.params.username } })
      .then(user => {
        if (!user) throw HttpErrors.UserNotFound;
        res.status(200).send({
          id: user.id,
          name: user.name,
          username: user.username,
          email: user.email
        });
      })
      .catch(err => {
        next(err);
      });
  })
  .get("/:username/courses", (req, res, next) => {
    User.findOne({ where: { username: req.params.username } })
      .then(user => {
        if (!user) throw HttpErrors.UserNotFound;
        return user.getCourses().then(courses => {
          if (courses.length === 0)
            throw new CreateError(404, "This user has no courses.");
          res.status(200).send(
            courses.map(course => {
              course.get_i18n(req.getLocale());
              return { id: course.id, course: course.course };
            })
          );
        });
      })
      .catch(err => {
        next(err);
      });
  })
  .get("/:username/subscriptions", (req, res, next) => {
    User.findOne({ where: { username: req.params.username } })
      .then(user => {
        if (!user) throw HttpErrors.UserNotFound;
        return user.getSubscriptions();
      })
      .then(subscriptions => {
        if (subscriptions.length === 0)
          throw new CreateError(404, "This user has no subscriptions.");
        res.status(200).send(
          subscriptions.map(subscription => {
            subscription.get_i18n(req.getLocale());
            return {
              id: subscription.id,
              name: subscription.course
            };
          })
        );
      })
      .catch(err => {
        next(err);
      });
  })
  .delete("/:id", hasPrivilege("manage_users"), (req, res, next) => {
    User.findById(req.params.id)
      .then(user => {
        if (!user) throw HttpErrors.UserNotFound;
        return user.destroy();
      })
      .then(() => {
        res.status(200).send();
      })
      .catch(err => {
        next(err);
      });
  });

export default router;
