import express from "express";
import CreateError from "http-errors";
import auth from "../passport/jwt";
import { Course, User, Category, Chapter } from "../models";
import AppErrors from "../error-handler";
import { PBAC as hasPrivilege } from "../access-control";

const router = express.Router();
const isValidCourseData = (req, res, next) => {
  if (
    req.body.course === undefined ||
    req.body.category === undefined ||
    req.body.chapters === undefined ||
    req.body.course === "" ||
    req.body.category === "" ||
    req.body.chapters === "" ||
    !(req.body.chapters instanceof Array)
  )
    next(AppErrors.BadFields);
  else next();
};
const isValidCourseName = (req, res, next) => {
  Course.findOne({
    where: { course: req.body.course }
  })
    .then(course => {
      if (course)
        throw new CreateError(406, "Course with this name allready exists");
      next();
    })
    .catch(err => {
      next(err);
    });
};
const canDoIt = (req, res, next) => {
  Course.findOne({
    where: { id: req.params.id },
    attributes: ["id"],
    include: [{ model: User, as: "Author", attributes: ["id"] }]
  })
    .then(course => {
      if (!course) throw AppErrors.CourseNotFound;
      if (course.Author.id === req.user.id) next();
      else hasPrivilege("edit_other_posts")(req, res, next);
    })
    .catch(err => {
      next(err);
    });
};

router
  .use(auth.initialize(), auth.authenticate())
  .get("/:id", (req, res, next) => {
    console.log(req.getLocale());
    Course.findOne({
      where: { id: req.params.id },
      attributes: ["course"],
      include: [
        {
          model: User,
          as: "Author",
          attributes: ["username", "name", "email"]
        },
        { model: Category, attributes: ["id", "category"] },
        { model: Chapter, attributes: ["id", "chapter"] }
      ]
    })
      .then(course => {
        if (!course) throw AppErrors.CourseNotFound;
        course.get_i18n(req.getLocale());
        course.Category.get_i18n(req.getLocale());
        res.status(200).send({
          name: course.course,
          Author: course.Author,
          Category: course.Category.category,
          Chapters: course.Chapters.map(chapter => {
            chapter.get_i18n(req.getLocale());
            return {
              id: chapter.id,
              name: chapter.chapter
            };
          })
        });
      })
      .catch(err => {
        next(err);
      });
  })
  .post(
    "/create",
    isValidCourseData,
    isValidCourseName,
    hasPrivilege("write_own_posts"),
    (req, res, next) => {
      const courseData = req.body;
      Course.create(
        {
          course: courseData.course,
          authorId: req.user.id,
          Chapters: courseData.chapters.map(chapter => ({ chapter }))
        },
        { include: [Chapter], language_id: req.getLocale() }
      )
        .then(course =>
          Category.findOrCreate({
            where: { category: courseData.category }
          }).spread(category => {
            course.setCategory(category);
            res.status(201).send("Course successfuly added.");
          })
        )
        .catch(err => {
          next(err);
        });
    }
  )
  .put("/:id/subscribe", (req, res, next) => {
    Course.findById(req.params.id)
      .then(course => {
        if (!course) throw AppErrors.CourseNotFound;
        return User.findById(req.user.id).then(user =>
          user.hasSubscriptions([course]).then(result => {
            if (result)
              throw new CreateError(406, "You are allready subscribed.");
            return user.addSubscriptions(course);
          })
        );
      })
      .then(subscription => {
        if (!subscription) throw new CreateError(406, "Cannot subscribe");
        res.status(201).send();
      })
      .catch(err => {
        next(err);
      });
  })
  .post("/:id/hassubscription", (req, res, next) => {
    Course.findById(req.params.id)
      .then(course => {
        if (!course) throw AppErrors.CourseNotFound;
        return User.findById(req.user.id).then(user =>
          user.hasSubscriptions([course])
        );
      })
      .then(subscription => {
        if (!subscription) throw new CreateError.NotFound();
        res.status(200).send();
      })
      .catch(err => {
        next(err);
      });
  })
  .put(
    "/:id/update",
    hasPrivilege("edit_own_posts"),
    canDoIt,
    (req, res, next) => {
      Course.findById(req.params.id)
        .then(course => {
          if (!req.body.course)
            throw new CreateError(400, "Give a course name");
          course.set_i18n(req.getLocale(), "course", req.body.course);
          res.status(201).send();
        })
        .catch(err => {
          next(err);
        });
    }
  )
  .delete("/:id", hasPrivilege("edit_own_posts"), canDoIt, (req, res, next) => {
    Course.destroy({ where: { id: req.params.id } })
      .then(() => {
        res.status(200).send();
      })
      .catch(err => {
        next(err);
      });
  });

export default router;
