import express from "express";
import CreateError from "http-errors";
import { Category } from "../models";
import HttpErrors from "../error-handler";
import auth from "../passport/jwt";
import { RBAC as hasRole } from "../access-control";

const router = express.Router();

router
  .use(auth.initialize(), auth.authenticate())
  .get("/:id", (req, res, next) => {
    Category.findById(req.params.id)
      .then(category => {
        if (!category) throw HttpErrors.CategoryNotFound;
        return category.getCourses();
      })
      .then(courses => {
        res.status(200).send(
          courses.map(course => {
            course.get_i18n(req.getLocale());
            return {
              id: course.id,
              course: course.course
            };
          })
        );
      })
      .catch(err => {
        next(err);
      });
  })
  .put("/:id/update", hasRole("Editor"), (req, res, next) => {
    Category.findById(req.params.id)
      .then(category => {
        if (!category) throw HttpErrors.CategoryNotFound;
        if (!req.body.category)
          throw new CreateError(400, "Give a category name");
        category.set_i18n(req.getLocale(), "category", req.body.category);
        res.status(201).send();
      })
      .catch(err => {
        next(err);
      });
  })
  .delete("/:id", hasRole("Admin"), (req, res, next) => {
    Category.destroy({ where: { id: req.params.id } })
      .then(() => {
        res.status(200).send();
      })
      .catch(err => {
        next(err);
      });
  });

export default router;
