import express from "express";
import * as jwt from "jsonwebtoken";
import CreateError from "http-errors";
import { User } from "../models";
import config from "../config/app";
import AppMailer from "../mail/nodemailer";

const router = express.Router();

router
  .put("/activate", (req, res, next) => {
    User.findOne({ where: { temporaryToken: req.body.token } })
      .then(user => {
        let decoded;
        if (!user) throw new CreateError(400, "Invalid token.");
        try {
          decoded = jwt.verify(req.body.token, config.key);
        } catch (e) {
          next(e);
        }
        if (!decoded) throw new CreateError(406, "Activation link has expired");
        return user.updateAttributes({
          isConfirmed: true,
          temporaryToken: false
        });
      })
      .then(user => {
        if (user) res.status(200).send("Account activated successfully.");
      })
      .catch(err => {
        next(err);
      });
  })
  .post("/resend", (req, res, next) => {
    User.findOne({
      where: { username: req.body.username },
      attributes: ["name", "email", "isConfirmed", "username", "password"]
    })
      .then(user => {
        if (!user) throw new CreateError(400, "Wrong login.");

        if (!req.body.password)
          throw new CreateError(400, "Enter your password.");
        if (!user.comparePasswords(req.body.password))
          throw new CreateError(400, "Wrong password.");
        if (user.isConfirmed)
          throw new CreateError(409, "User is allready activated.");
        user.updateAttributes({
          temporaryToken: jwt.sign(
            { username: user.username, email: user.email },
            config.key,
            { expiresIn: "24h" }
          )
        });
      })
      .then(user => {
        if (user) {
          AppMailer.sendMail({
            to: `${user.name}, <${user.email}>`,
            subject: "Account validation",
            template: "validation",
            context: {
              name: user.name,
              appName: config.name,
              server: config.server,
              temporarytoken: user.temporaryToken
            }
          });
          res.status(201).send("Validation link has sended to your email.");
        }
      })
      .catch(err => {
        next(err);
      });
  });

export default router;
