import express from "express";
import CreateError from "http-errors";
import slugify from "slugify";
import { RBAC as hasRole } from "../access-control";
import auth from "../passport/jwt";
import { Role, User, Privilege } from "../models";
import HttpErrors from "../error-handler";

const router = express.Router();

const isValidRoleData = (req, res, next) => {
  if (!req.body.description) throw new CreateError(400, "No role description.");
  if (!req.body.role) throw new CreateError(400, "No role name.");
  Role.findOne({ where: { role: req.body.role } })
    .then(role => {
      if (role) throw new CreateError(401, "This role already exists.");
      next();
    })
    .catch(err => {
      next(err);
    });
};

const isValidPrivilegeData = (req, res, next) => {
  if (!req.body.privilege) throw new CreateError(400, "No privilege name.");
  if (!req.body.description)
    throw new CreateError(400, "No privilege description.");
  Privilege.findOne({
    where: { privilege: slugify(req.body.privilege, "_").toLowerCase() }
  })
    .then(privilege => {
      if (privilege)
        throw new CreateError(400, "This privilege already exists.");
      next();
    })
    .catch(err => {
      next(err);
    });
};

router
  .use(auth.initialize(), auth.authenticate(), hasRole("Admin"))
  .get("/all", (req, res, next) => {
    Role.findAll()
      .then(roles => {
        res.status(200).send(
          roles.map(role => ({
            id: role.id,
            role: role.role,
            description: role.description
          }))
        );
      })
      .catch(err => {
        next(err);
      });
  })
  .get("/get/:userid", (req, res, next) => {
    User.findById(req.params.userid)
      .then(user => {
        if (!user) throw HttpErrors.UserNotFound;
        return user.getRoles();
      })
      .then(roles => {
        if (roles.length === 0)
          throw new CreateError(404, "This user has no roles.");
        res.status(200).send(
          roles.map(role => ({
            id: role.id,
            role: role.role,
            description: role.description
          }))
        );
      })
      .catch(err => {
        next(err);
      });
  })
  .get("/:id/privileges", (req, res, next) => {
    Role.findById(req.params.id)
      .then(role => {
        if (!role) throw new CreateError(404, "Role not found");
        return role.getPrivileges();
      })
      .then(privileges => {
        if (privileges.length === 0)
          throw new CreateError(404, "This role has no privileges");
        res.status(200).send(
          privileges.map(privilege => ({
            privilege: privilege.privilege,
            description: privilege.description
          }))
        );
      })
      .catch(err => {
        next(err);
      });
  })
  .post("/add", isValidRoleData, (req, res, next) => {
    Role.create({
      role: req.body.role,
      description: req.body.description
    })
      .then(role => {
        if (!role) throw new CreateError(500, "Role not created.");
        res.status(201).send();
      })
      .catch(err => {
        next(err);
      });
  })
  .post("/add/privilege", isValidPrivilegeData, (req, res, next) => {
    Privilege.create({
      privilege: slugify(req.body.privilege, "_").toLowerCase(),
      description: req.body.description
    })
      .then(privilege => {
        if (!privilege)
          throw new CreateError(500, "Privilege cannot be added.");
        res.status(201).send();
      })
      .catch(err => {
        next(err);
      });
  })
  .put("/:id/addprivileges", (req, res, next) => {
    let setPriv;
    if (req.body.privilege) setPriv = [].concat(req.body.privilege);
    else if (req.body.privileges) setPriv = [].concat(req.body.privileges);
    Privilege.findAll()
      .then(privileges => {
        if (privileges.length === 0)
          throw new CreateError(404, "No privileges in database.");
        if (setPriv.length === 0)
          throw new CreateError(400, "No privileges provided.");
        setPriv.forEach(priv => {
          if (privileges.map(p => p.privilege).indexOf(priv) === -1)
            throw new CreateError(400, "Invalid privilege given.");
        });
        return Role.findById(req.params.id).then(role => {
          if (!role) throw new CreateError(404, "Role not found.");
          privileges.forEach(priv => {
            if (setPriv.indexOf(priv.privilege) !== -1) role.addPrivilege(priv);
          });
          res.status(201).send();
        });
      })
      .catch(err => {
        next(err);
      });
  })
  .put("/add/:userid", (req, res, next) => {
    let setRoles;
    if (req.body.role) setRoles = [].concat(req.body.role);
    else if (req.body.roles) setRoles = [].concat(req.body.roles);
    Role.findAll()
      .then(roles => {
        if (roles.length === 0)
          throw new CreateError(404, "No roles in database.");
        if (setRoles.length === 0)
          throw new CreateError(400, "No roles provided");
        setRoles.forEach(forSet => {
          if (roles.map(role => role.role).indexOf(forSet) === -1)
            throw new CreateError(400, "Invalid role given.");
        });
        return User.findById(req.params.userid).then(user => {
          if (!user) throw HttpErrors.UserNotFound;
          roles.forEach(role => {
            if (setRoles.indexOf(role.role) !== -1) user.addRole(role);
          });
          res.status(201).send();
        });
      })
      .catch(err => {
        next(err);
      });
  })
  .delete("/:userid", (req, res, next) => {
    let delRoles;
    if (req.body.role) delRoles = [].concat(req.body.role);
    else if (req.body.roles) delRoles = [].concat(req.body.roles);
    Role.findAll()
      .then(roles => {
        if (roles.length === 0)
          throw new CreateError(404, "No roles in database.");
        if (delRoles.length === 0)
          throw new CreateError(404, "No roles provided");
        delRoles.forEach(forSet => {
          if (roles.map(role => role.role).indexOf(forSet) === -1)
            throw new CreateError(400, "Invalid role given.");
        });
        return User.findById(req.params.userid).then(user => {
          if (!user) throw HttpErrors.UserNotFound;
          roles.forEach(role => {
            if (delRoles.indexOf(role.role) !== -1) user.removeRole(role);
          });
          res.status(204).send();
        });
      })
      .catch(err => {
        next(err);
      });
  })
  .delete("/:id/privilege", (req, res, next) => {
    let delPriv;
    if (req.body.privilege) delPriv = [].concat(req.body.privilege);
    else if (req.body.privileges) delPriv = [].concat(req.body.privileges);
    Privilege.findAll()
      .then(privileges => {
        if (privileges.length === 0)
          throw new CreateError(404, "No privileges in database.");
        if (delPriv.length === 0)
          throw new CreateError(404, "No privileges provided");
        delPriv.forEach(forSet => {
          if (privileges.map(p => p.privilege).indexOf(forSet) === -1)
            throw new CreateError(400, "Invalid privilege given.");
        });
        return Role.findById(req.params.id).then(role => {
          if (!role) throw new CreateError(404, "Role not found.");
          privileges.forEach(priv => {
            if (delPriv.indexOf(priv.privilege) !== -1)
              role.removePrivilege(priv);
          });
          res.status(204).send();
        });
      })
      .catch(err => {
        next(err);
      });
  });

export default router;
