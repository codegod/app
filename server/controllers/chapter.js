import express from "express";
import CreateError from "http-errors";
import auth from "../passport/jwt";
import { Chapter, Course, User } from "../models";
import { PBAC as hasPrivilege } from "../access-control";
import HttpErrors from "../error-handler";

const router = express.Router();
const isValidChapterData = fields => {
  if (
    fields.chapter === "" ||
    fields.chapter === undefined ||
    fields.courseId === "" ||
    fields.courseId === undefined
  )
    throw HttpErrors.BadFields;
  return true;
};
const canDoIt = (req, res, next) => {
  Chapter.findOne({
    where: { id: req.params.id },
    attributes: ["id"],
    include: [
      {
        model: Course,
        attributes: ["id"],
        include: [{ model: User, as: "Author", attributes: ["id"] }]
      }
    ]
  })
    .then(chapter => {
      if (!chapter) throw HttpErrors.ChapterNotFound;
      if (chapter.Course.Author.id === req.user.id) next();
      else hasPrivilege("edit_other_posts")(req, res, next);
    })
    .catch(err => {
      next(err);
    });
};

router
  .use(auth.initialize(), auth.authenticate())
  .get("/:id", (req, res, next) => {
    Chapter.findById(req.params.id)
      .then(chapter => {
        if (!chapter) throw HttpErrors.ChapterNotFound;
        return chapter.getLessons();
      })
      .then(lessons => {
        if (!lessons || lessons.length === 0)
          throw new CreateError(404, "No lessons found for this chapter");
        res.status(200).send(
          lessons.map(lesson => {
            lesson.get_i18n(req.getLocale());
            return {
              id: lesson.id,
              name: lesson.lesson,
              address: lesson.address
            };
          })
        );
      })
      .catch(err => {
        next(err);
      });
  })
  .post("/create", hasPrivilege("write_own_posts"), (req, res, next) => {
    const chapterData = req.body;
    if (isValidChapterData(chapterData))
      Course.findById(chapterData.courseId)
        .then(course => {
          if (!course) throw new CreateError(404, "Course not found.");
          return Chapter.create(
            {
              chapter: chapterData.chapter
            },
            { language_id: req.getLocale() }
          ).then(chapter => {
            course.addChapters(chapter);
            res.status(201).send();
          });
        })
        .catch(err => {
          next(err);
        });
  })
  .put(
    "/:id/update",
    hasPrivilege("edit_own_posts"),
    canDoIt,
    (req, res, next) => {
      Chapter.findById(req.params.id)
        .then(chapter => {
          if (!req.body.chapter)
            throw new CreateError("Give a name for the chapter");
          chapter.set_i18n(req.getLocale(), "chapter", req.body.chapter);
          res.status(201).send();
        })
        .catch(err => {
          next(err);
        });
    }
  )
  .delete("/:id", hasPrivilege("edit_own_posts"), canDoIt, (req, res, next) => {
    Chapter.destroy({ where: { id: req.params.id } })
      .then(() => {
        res.status(200).send();
      })
      .catch(err => {
        next(err);
      });
  });

export default router;
