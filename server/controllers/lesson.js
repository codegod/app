import express from "express";
import fileUpload from "express-fileupload";
import CreateError from "http-errors";
import path from "path";
import uuidv4 from "uuid/v4";
import auth from "../passport/jwt";
import { Lesson, Chapter, Attachment, Comment, User, Course } from "../models";
import { PBAC as hasPrivileges } from "../access-control";
import HttpErrors from "../error-handler";

const router = express.Router();
const isValidLessonData = (reqFields, lessonFields) => {
  if (!reqFields.files) throw new CreateError(400, "No files were uploaded.");
  if (!reqFields.files.lesson || reqFields.files.lesson instanceof Array)
    throw new CreateError(400, "No lesson file provided or too many files.");
  if (!lessonFields.chapterId) throw new CreateError(400, "No chapterId.");
  if (!lessonFields.lessonTitle) throw new CreateError(400, "No lesson title.");
  return true;
};
const moveAttachments = (attachments, lessonTitle) =>
  [].concat(attachments).map(attachment => {
    const newFile = {
      name: `attachment-${uuidv4()}`,
      ext: path.extname(attachment.name),
      path: `/uploads/attachments/`,
      pref: "/.."
    };
    const aAddress = path.join(newFile.path, newFile.name + newFile.ext);
    attachment.mv(path.join(__dirname, newFile.pref, aAddress), err => {
      if (err) throw new CreateError.InternalServerError();
    });
    return {
      address: aAddress,
      description: `${lessonTitle} - ${path.basename(
        attachment.name,
        newFile.ext
      )}`
    };
  });
const moveLesson = lesson => {
  const newFile = {
    name: `lesson-${uuidv4()}`,
    ext: path.extname(lesson.name),
    path: `/uploads/lessons/`,
    pref: "/.."
  };
  lesson.mv(
    path.join(
      __dirname,
      newFile.pref,
      newFile.path,
      newFile.name + newFile.ext
    ),
    err => {
      if (err) throw new CreateError.InternalServerError();
    }
  );
  return newFile;
};
const isValidFiles = (req, res, next) => {
  const lessonExt = ["mp4", "jpg", "jpeg", "png"];
  const attachExt = ["doc", "docx", "json", "rar"];
  if (isValidLessonData(req, req.body)) {
    if (
      lessonExt.indexOf(
        req.files.lesson.name.split(".")[
          req.files.lesson.name.split(".").length - 1
        ]
      ) === -1
    )
      throw new CreateError(400, "Invalid lesson extension.");
    []
      .concat(req.files.attachments)
      .map(
        attachment =>
          attachment.name.split(".")[attachment.name.split(".").length - 1]
      )
      .forEach(attachment => {
        if (attachExt.indexOf(attachment) === -1)
          throw new CreateError(400, "Invalid attachment extension.");
      });
  }
  next();
};
const canDoIt = (req, res, next) => {
  Lesson.findOne({
    where: { id: req.params.id },
    include: [
      {
        model: Chapter,
        attributes: ["id"],
        include: [
          {
            model: Course,
            attributes: ["id"],
            include: [{ model: User, as: "Author", attributes: ["id"] }]
          }
        ]
      }
    ]
  })
    .then(lesson => {
      if (!lesson) throw HttpErrors.LessonNotFound;
      if (lesson.Chapter.Course.Author.id === req.user.id) next();
      else hasPrivileges("edit_other_posts")(req, res, next);
    })
    .catch(err => {
      next(err);
    });
};

router
  .use(auth.initialize(), auth.authenticate())
  .get("/:id", (req, res, next) => {
    Lesson.findOne({
      where: { id: req.params.id },
      attributes: ["lesson", "address"],
      include: [
        { model: Attachment, attributes: ["address", "description"] },
        {
          model: Comment,
          attributes: ["comment"],
          include: [
            {
              model: User,
              as: "Author",
              attributes: ["name", "username", "email"]
            }
          ]
        },
        { model: Chapter, attributes: ["id", "chapter"] }
      ]
    })
      .then(lesson => {
        if (!lesson) throw HttpErrors.LessonNotFound;
        lesson.get_i18n(req.getLocale());
        lesson.Chapter.get_i18n(req.getLocale());
        res.status(200).send({
          lesson: lesson.lesson,
          address: lesson.address,
          Attachments: lesson.Attachments.map(attachment => {
            attachment.get_i18n(req.getLocale());
            return {
              address: attachment.address,
              description: attachment.description
            };
          }),
          Comments: lesson.Comments,
          Chapter: lesson.Chapter.chapter
        });
      })
      .catch(err => {
        next(err);
      });
  })
  .post(
    "/create",
    fileUpload({ limits: { fileSize: 50 * 1024 * 1024 } }),
    hasPrivileges(["write_own_posts", "upload_files"]),
    isValidFiles,
    (req, res, next) => {
      const lessonData = req.body;
      if (isValidLessonData(req, lessonData))
        Chapter.findById(lessonData.chapterId)
          .then(chapter => {
            if (!chapter) throw new CreateError(404, "Chapter not found.");
            const newFile = moveLesson(req.files.lesson);
            const attachments = moveAttachments(
              req.files.attachments,
              lessonData.lessonTitle
            );
            return Lesson.create(
              {
                lesson: lessonData.lessonTitle,
                address: path.join(newFile.path, newFile.name + newFile.ext),
                ChapterId: lessonData.chapterId,
                Attachments: attachments
              },
              { include: [Lesson.hasMany(Attachment)] }
            );
          })
          .then(lesson => {
            if (!lesson) throw new CreateError.InternalServerError();
            res.status(201).send("Lesson created.");
          })
          .catch(err => {
            next(err);
          });
    }
  )
  .put(
    "/:id/update",
    hasPrivileges(["edit_own_posts"]),
    canDoIt,
    (req, res, next) => {
      Lesson.findById(req.params.id)
        .then(lesson => {
          if (!req.body.lesson)
            throw new CreateError(400, "Give a new lesson title");
          lesson.set_i18n(req.getLocale(), "lesson", req.body.lesson);
          res.status(201).send();
        })
        .catch(err => {
          next(err);
        });
    }
  )
  .delete(
    "/:id",
    hasPrivileges(["edit_own_posts"]),
    canDoIt,
    (req, res, next) => {
      Lesson.destroy({ where: { id: req.params.id } })
        .then(() => {
          res.status(200).send();
        })
        .catch(err => {
          next(err);
        });
    }
  );

export default router;
