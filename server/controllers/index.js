import express from "express";
import users from "./users";
import chapter from "./chapter";
import course from "./course";
import email from "./email";
import lesson from "./lesson";
import category from "./category";
import role from "./role";

const router = express.Router();

router.use("/email", email);
router.use("/category", category);
router.use("/users", users);
router.use("/course", course);
router.use("/chapter", chapter);
router.use("/lesson", lesson);
router.use("/role", role);

export default router;
