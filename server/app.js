import express from "express";
import cors from "cors";
import * as bodyParser from "body-parser";
import Sequelize from "sequelize";
import i18n from "i18n";
import fs from "fs";
import path from "path";
import config from "./config/app";
import controllers from "./controllers";

require("babel-register");

const port = config.port;
const app = express();
if (!fs.existsSync(path.join(__dirname, "uploads"))) {
  fs.mkdirSync(path.join(__dirname, "uploads"));
}
if (!fs.existsSync(path.join(__dirname, "uploads", "lessons"))) {
  fs.mkdirSync(path.join(__dirname, "uploads", "lessons"));
}
if (!fs.existsSync(path.join(__dirname, "uploads", "attachments"))) {
  fs.mkdirSync(path.join(__dirname, "uploads", "attachments"));
}
i18n.configure({
  locales: ["en", "fr", "md"],
});

app.use(bodyParser.json());
app.use(cors());
app.use(i18n.init);
app.use(controllers);
app.use("/uploads", express.static(path.join(__dirname, "uploads")));
app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "index.html"));
});
app.use((err, req, res, next) => {
  if (err) {
    if (err instanceof Sequelize.ValidationError)
      res.status(400).send(err.errors[0].message);
    else res.status(err.status || 400).send(err.message);
  } else next();
});
app.listen(port, err => {
  if (err) {
    console.error(`Error occured: ${err}`);
  } else {
    console.log(`Listening on port: ${port}`);
  }
});
