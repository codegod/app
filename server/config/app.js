const config = {
  key: "otorinolaringolog",
  port: 8082,
  dns: "localhost",
  name: "App"
};

config.server = `http://${config.dns}:${config.port}`;

export default config;
